from mininet.topo import Topo
from mininet.net import Mininet                                                                        
from mininet.node import Controller, RemoteController, Node 
from mininet.cli import CLI 
from mininet.log import setLogLevel, info 
from mininet.link import Link, Intf






class Topology( Topo ):

    def __init__( self ):

        Topo.__init__( self )

   
        server = self.addHost( 'h1', ip='0.0.0.0' )
        victimClient = self.addHost( 'h2', ip='0.0.0.0' )
        attacker = self.addHost( 'h3', ip='0.0.0.0' )
        switch = self.addSwitch( 's1' )

        self.addLink( server, switch )
        self.addLink( victimClient, switch )
        self.addLink( attacker, switch )

def aggNet():
    CONTROLLER_IP='127.0.0.1'

    topo = Topology()
    net = Mininet( topo=topo, build=False, controller=None)
    net.addController( 'c0', controller=RemoteController, ip=CONTROLLER_IP, port=6633 )
 

    net.start()
    CLI( net ) 
    net.stop() 



if __name__ == '__main__':
    setLogLevel( 'info' )
    aggNet()


# The help has been taken from mininet walkthrough as provided
